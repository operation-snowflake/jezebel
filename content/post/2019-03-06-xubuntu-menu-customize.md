---
title: Xubuntu menu customization
subtitle: Understanding the Xubuntu menu system
date: 2019-03-06
tags: ["xubuntu", "menu"]
---

#### Menu terminology
* .desktop: Describes information about an application. These files are used for application launchers and for creating menus of applications that can be launched.
* .directory: Provides a localized name and an icon for a submenu.  
* .menu: Defines how to construct a UI hierarchy of applications, typically displayed as a menu.  
* Root Menu: X-Xfce-Toplevel  
* Category: Categories determine where in the UI a .desktop definition is displayed. Valid categories include:  
  * GNOME  
  * GTK  
  * Settings  
  * X-GNOME-Settings-Panel  
  * X-GNOME-PersonalSettings  
  * DesktopSettings  
  * X-XFCE  


#### File locations 
Xfce default (system-wide): `/etc/xdg/menus/xfce-applications.menu`  
```bash
    <Menu>
        <Name>Education</Name>
        <Directory>xfce-education.directory</Directory>
        <Include>
            <Category>Education</Category>
        </Include>
    </Menu>
```

Xubuntu default (system-wide): `/etc/xdg/xdg-xubuntu/menus/xfce-applications.menu`  
```bash
    <Menu>
        <Name>Education</Name>
        <Directory>xfce-education.directory</Directory>
        <Include>
            <Category>Education</Category>
        </Include>
    </Menu>
```

Local (per user): `~/.config/menus/xfce-applications.menu`  
```bash
    <Menu>
        <Name>Education</Name>
        <Directory>xfce-education.directory</Directory>
	    <Layout>
            <Merge type="menus" />
            <Filename>libreoffice-math.desktop</Filename>
            <Merge type="files" />
        </Layout>
    </Menu>
```

#### Anatomy of .desktop files
Format: Plaintext  
Structure:
```bash
[Desktop Entry]
Name= # the name of the application presented to the UI
Comment= # commonly used to present mouse-over text to the UI
Categories= # semicolon-delimited list of indicating where the shortcut appears
Keywords= # 
Exec= # path to the executable target  
Icon= # path to the icon presented to the UI
Terminal= # whether the file executes in a terminal 
Type= # 
```

#### Anotomy of xfce-applications.menu
Format: XML  

```bash
cat ${XDG_CONFIG_HOME:-~/.config}/menus/xfce-applications.menu

<MergeFile type="parent">/etc/xdg/xdg-xubuntu/menus/xfce-applications.menu</MergeFile>
    <Menu>
        <Name>Accessories</Name>
        <Directory>xfce-accessories.directory</Directory>
        <Layout>
            <Merge type="menus" />
            <Filename>xfce4-run.desktop</Filename>
            <Filename>xfce4-appfinder.desktop</Filename>
            <Separator />
            <Filename>mugshot.desktop</Filename>
            <Filename>xfce4-about.desktop</Filename>
            <Filename>xfce4-accessibility-settings.desktop</Filename>
            ...
            <Merge type="files" />
        </Layout>
    </Menu>
```

#### Create a new sub-menu  
1. Create a *.desktop* file to define the menu item  
1. Create a *.directory* file to define the sub-menu title and icon
1. Create a *.menu* file to link .desktop and .directory togther


[freedesktop.org Desktop Menu Specification](https://specifications.freedesktop.org/menu-spec/menu-spec-1.0.html#category-registry)


## Set the Applications menu as desktop right-click
1. Open `Settings Manager > Icons`.
1. Set `Icon Type` to `None`.
